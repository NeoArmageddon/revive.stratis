Neo's Revive Script
29.12.2013

Beta disclaimer
----------------
This script is released as a beta. It was tested in singleplayer, hosted and dedicated multiplayer. But there are also a lot of scenarios I were not able to test yet.
If you found a bug or want to give general feedback, feel free to contact me in the BI-forums.


Description
----------------
This script adds revive functionality to your A3-mission or mod.
Instead of dying a unit will become unconscious (with ragdolling). Other players are able to revive a unconscious player.
While unconscious a player is able to recover (triggering W or shift+W) and roll over. The player is then able to slowly crawl and empty his loaded magazine (wihtout scope).


Included
----------------
Minimalistic samplemission including the revive.


Requirements
-----------------
Arma 3 v1.08 and higher (Devbranch compatible)


Changelog
-----------------
29.12.2013 - Beta release.


Installation and usage
-----------------
If you get the mission from Git, make sure all files are in a folder called revive.stratis in your editors mission folder.

You can use the mission for testing the revive in SP and MP and as base for your own mission. If you want to use the scripts in an existing mission,
copy the function and include folder to your mission. Copy the contents of the init.sqf to your own missions init and add "#include "include\functions.hpp"
to your missions description.ext. Make sure you don't have a cfgFunctions section or include the function.hpp in it.
Add a gamelogic called "center" to the center of the map.

You can use the Revive with respawn enabled or disabled. In general the revive will prevent a unit from dying but scripts and the respawn button can still kill a unit.
The revivescript should be able to handle a respawn correctly.

If you have problems installing the revive, feel free to ask me for help.


Knwon bugs
-----------------
If somebody dies in a car and gets ejected the animation is sometimes stuck after revive. Just shot the player again and everything should be fine.


Contact
-----------------
BI Forums: NeoArmageddon
Email: neo [AT] armed-tactics.de
Website and forum at www.armed-tactics.de or www.modfact.net


Credits and Thanks
-----------------
BIS - OFP to ArmA3
NeoArmageddon - Scripting
tpw - Idea for emulating a ragdoll
Scruffy, Darcy, Maike, DERyoshi, MemphisBelle, Vormulac and Freshman - Testing the script.


License
-----------------

This addon/script and all of it's content is released unter the Creative Commons Attribution Non-Commercial Share Alike Licence.
This license lets you remix, tweak, and build upon this work non-commercially, as long as you credit the author and license your new creations under the identical terms. Others can download and redistribute this work just like the by-nc-nd license, but you can also translate and make remixes based on this work. All new work based on this will carry the same license, so any derivatives will also be non-commercial in nature.


Disclaimer
-----------------
I take no responsibility for (im)possible damage to your game/system that may be caused by installation of this script.
