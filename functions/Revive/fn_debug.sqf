/*
        Neo's Revive script.
	Author: Dennis Meyer aka NeoArmageddon
        
        License and permissions:
        Please see readmefile

	Description:
	Posts a debug message

	Parameter(s):
		0: A Message for debugging
             
	Returns:
	Void
*/

private["_msg","_threshold"];
_msg = [_this, 0, "Missing Debugstring", [""]] call BIS_fnc_param;

if(false) then {
    player globalchat format["%1",_msg];
};


