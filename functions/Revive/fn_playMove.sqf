/*
        Neo's Revive script.
	Author: Dennis Meyer aka NeoArmageddon
        
        License and permissions:
        Please see readmefile

	Description:
	Plays a animation on a unit. Used for MP syncronisation of animations.

	Parameter(s):
		0: Unit to play the animation
                1: Animation

             
	Returns:
	Void
*/

private["_unit","_anim"];

_unit = _this select 0;
_anim = _this select 1;
if(!isNull _unit) then {
    _unit playmovenow _anim;
};

