/*
        Neo's Revive script.
	Author: Dennis Meyer aka NeoArmageddon
        
        License and permissions:
        Please see readmefile

	Description:
	Switch a unit to a animation. Used for MP syncronisation of animations.

	Parameter(s):
		0: Unit to switch the animation
                1: Animation

             
	Returns:
	Void
*/

private["_unit","_anim"];

_unit = _this select 0;
_anim = _this select 1;

if(!isNull _unit) then {
    _unit switchMove _anim;
};

