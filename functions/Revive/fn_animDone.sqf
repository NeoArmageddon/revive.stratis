/*
        Neo's Revive script.
	Author: Dennis Meyer aka NeoArmageddon
        
        License and permissions:
        Please see readmefile

	Description:
	Function called by a animDone eventhandler to track changes in animation while healing

	Parameter(s):
		0: Unit the handler is attached to
                1: Animation
             
	Returns:
	Void
*/

private["_unit","_anim","_injured","_ended","_success"];
_unit = _this select 0;
_anim = _this select 1;
if(count(_unit getvariable "AT_isHealing")>0 && _anim == "AinvPknlMstpSnonWrflDr_medic2") then {
		if(!((_unit getvariable "AT_isHealing") select 1)) then {
			_injured = (_unit getvariable "AT_isHealing") select 0;
			_ended = (_unit getvariable "AT_isHealing") select 1;
			_success = (_unit getvariable "AT_isHealing") select 2;
			_unit setvariable ["AT_isHealing",[_injured,true,true],true];
		};
};