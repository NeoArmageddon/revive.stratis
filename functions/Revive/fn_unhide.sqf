/*
        Neo's Revive script.
	Author: Dennis Meyer aka NeoArmageddon
        
        License and permissions:
        Please see readmefile

	Description:
	Function to unhide a unit which was hide via at_fnc_hide.

	Parameter(s):
		0: Unit to unhide

             
	Returns:
	Void
*/

private["_unit"];
_unit = _this select 0;

_unit enableSimulation true;
_unit hideobject false;