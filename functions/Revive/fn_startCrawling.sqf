/*
        Neo's Revive script.
	Author: Dennis Meyer aka NeoArmageddon
        
        License and permissions:
        Please see readmefile

	Description:
	Called by a animChanged handler while a unit is unconscious. Tracks if the unit rolled to the belly.
        Randomly let's the unit roll back on the back.

	Parameter(s):
		0: Unit
                1: Animation

             
	Returns:
	Void
*/

private["_unit","_anim","_injured","_ended","_success"];
_unit = _this select 0;
_anim = _this select 1;
if(_anim == "amovppnemstpsraswrfldnon_injured") then {
    if(!(_unit getvariable "AT_isCrawling")) then {
	_unit setcaptive false;
        _unit setvariable ["AT_isCrawling",true,false];
        [_unit] spawn {
            private["_unit","_endtime"];
            _unit = _this select 0;
            _endtime = time + random 10 + 5;
            waituntil{time>_endtime || !(_unit getvariable "AT_isCrawling")};
             if((_unit getvariable "AT_isCrawling") && !(_unit getvariable "AT_isConscious")) then {
               _unit setcaptive true;
               [[_unit,"AinjPpneMstpSnonWrflDnon"],"at_fnc_playMove",true] call BIS_fnc_MP;
               _unit setvariable ["AT_isCrawling",false,false];
             }
        };
    };
};
