/*
        Neo's Revive script.
	Author: Dennis Meyer aka NeoArmageddon
        
        License and permissions:
        Please see readmefile

	Description:
	Function to hide a unit completely. Used for faked ragdoll.

	Parameter(s):
		0: Unit to hide

             
	Returns:
	Void
*/

private["_unit"];
_unit = _this select 0;

_unit enableSimulation false;
_unit hideobject true;