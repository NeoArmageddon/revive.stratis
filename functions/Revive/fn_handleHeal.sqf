/*
        Neo's Revive script.
	Author: Dennis Meyer aka NeoArmageddon
        
        License and permissions:
        Please see readmefile

	Description:
	Function called by a handleHeal eventhandler. Unused at the moment

	Parameter(s):
		0: Unit the handler is attached to
                1: Healing unit
                2: Is the healer a medic?
             
	Returns:
            Boolflag if healing was succesfull
*/

private["_unit","_reviver","_changeEH","_doneEH","_abortAction","_script","_return"];

_unit = _this select 0;
_reviver = _this select 1;
_canHeal = _this select 2;

if(!(_unit getvariable "AT_isConscious") && _canHeal) then {
    _return = true;
    _script = [_unit,_reviver] spawn at_fnc_revive;
    AISFinishHeal [_unit, _reviver, _canHeal];
 } else {
    _return = false;
 };
_return;
